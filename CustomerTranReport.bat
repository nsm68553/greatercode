@echo off

SETLOCAL
SET basepath=%$TRANSACTION_PROCESSING%
SET filename=finance_customer_transactions-
SET fileext=.csv
SET pending=%basepath%\pending\
SET processed=%basepath%\processed\

rem : check how many files are in pending directory

set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  

if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)

rem : There is no files in the directory.  Check again in 2 minutes - iteration 1

echo "File not found.  Wait 2 minutes"
timeout 120 > nul
set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  
if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)

rem : There is no files in the directory.  Check again in 2 minutes - iteration 2

echo "File not found.  Wait 2 minutes"
timeout 120 > nul
set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  
if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)


rem : There is no files in the directory.  Check again in 2 minutes - iteration 3

echo "File not found.  Wait 2 minutes"
timeout 120 > nul
set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  
if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)

rem : There is no files in the directory.  Check again in 2 minutes - iteration 4

echo "File not found.  Wait 2 minutes"
timeout 120 > nul
set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  
if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)

rem : There is no files in the directory.  Check again in 2 minutes - iteration 5

echo "File not found.  Wait 2 minutes"
timeout 120 > nul
set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  
if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)

rem : There is no files in the directory.  Check again in 2 minutes - iteration 6

echo "File not found.  Wait 2 minutes"
timeout 10 > nul
set count=0 
for %%x in (%pending%%filename%*%fileext%) do @(set /a count+=1 > nul)

if %count% GTR 1 (
 
   echo "There should only be one file in the pending directory."
   echo "Please check files for duplicates."
   echo "If all files need to be processed, please add them one at a time to the pending directory and run script"
   Exit /B 1
)  
if %count% EQU 1 (
  FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
   GOTO :EOF
)

if %count% EQU 0 (
   echo "Input file has not been received.  Please check"
   EXIT /B 1
)

FOR %%f IN (%pending%%filename%*%fileext%) DO CALL :processfile "%%f"
GOTO :EOF

:processfile

rem : extract the filename from the directory

SET srcfile=%1
SET "name=%~n1"
SET "name=%name:*/=%"
SET inputfilename=%name%%fileext%

rem : run program to produce report

echo "Processing %inputfilename%"
java -cp . CustomerTranRpt %inputfilename% %basepath%

if %ERRORLEVEL% NEQ 0 (

echo "Script failed - please check errorlogs and rerun"
Exit /B 1
)

rem :  Script successful - move file from pending directory to processed directory

move %srcfile% %processed%
if %ERRORLEVEL% NEQ 0 (
echo "Unable to move file %srcfile% to %processed%"
Exit /B 1
)

: rem end function
Exit /B %ERRORLEVEL%