/***********************************************************************************************************************************
 * CustomerTranRpt program reads a file of transactions in the format of <account number>, <amount>
 * It checks that the account number is valid (ie a integer number) and the amount is valid (ie decimal (including - signs if debit)
 * The program adds all the credit amounts together and all the debit amounts together to obtain a total sum
 * The program also counts how many individual accounts were in the file and how many records were skipped
 * FYI - any headers in the file will be treated as "skipped" transactions.
 * Usage: CustomerTranRpt <input filename> <base path>
 **************************************************************************************/

import java.text.DateFormat;  
import java.util.Date; 
import java.io.*;
import java.text.*;

class CustomerTranRpt {

public static boolean CheckDecimal(String str) {

    // This function will accept a string and determine whether it contains a valid decimal number

    if (str == null || str.isEmpty()) 
    {
        return false;
    }

    int i = 0;
    int dec = 0;

                  // check if first character is a negative sign - if so, make sure that it doesn't just contain a negative sign.
                  // ie it contains characters after the negative sign
    if (str.charAt(0) == '-') 
    {
        if (str.length() > 1) 
        {
            i++;
        } 
        else 
        {
                 // string only contains '-' - therefore, it is not a valid amount
            return false;
        }
    }
                // check remaining characters - they must be a digit or a ".".  There must only be one "." in the string
  
    for (; i < str.length(); i++) 
    {
        if (str.charAt(i) == '.')
        {
           if (dec == 0)
           {
              dec = 1;
           }
           else
           {
                    // there is more than 1 decimal point in the number - therefore, not valid amount
              return false;
           } 
        }
        else
        {
           if (!Character.isDigit(str.charAt(i))) 
           {
                      // it is not a decimal point or a digit.  Therefore, not valid amount
               return false;
           }
        }
    }
    return true;
}

public static boolean CheckNumber(String str) {

   // This function will accept a string and determine whether it contains a valid integer number

    if (str == null || str.isEmpty()) 
    {
               // string is empty - therefore, not valid number
        return false;
    }

    int i = 0;

    for (; i < str.length(); i++) 
    {
        
      if (!Character.isDigit(str.charAt(i))) 
      {
              // string contains non-numeric characters
         return false;
      }
    }
    return true;
}


    public static void main(String[] args) {

    // define report output variables

    int AccNo_count = 0;           // Number of distinct accounts
    int Skip_count = 0;            // Number of intput transactions skipped due to corruption
    double Debit_sum = 0.0;        // Sum of debit transactions
    double Credit_sum = 0.0;       // Sum of credit transactions

   // arrays for holding account numbers

   String [] accounts1 = new String[500000];
   String [] accounts2 = new String[500000];
   String [] accounts3 = new String[500000];
   String [] accounts4 = new String[500000];
   String [] accounts5 = new String[500000];
   String [] accounts6 = new String[500000];
   String [] accounts7 = new String[500000];
   String [] accounts8 = new String[500000];
   String [] accounts9 = new String[500000];
   String [] accounts0 = new String[500000]; 

   // account array size - initialise all to 0.

   int [] arraysize = {0,0,0,0,0,0,0,0,0,0};
    
    // define file variables

    String InputFileName = args[0];                   //Input file name from input file argument.
    String BasePath = args[1];                        // base directory path holding input and output directories
    int IFlength = InputFileName.length();            // Filename length
    String IFdatetime = "";                           // extract date and time from input directory - to put on output directory
    String filepath = "";                             // input file directory
    
  
    // check if input file is correct - it should be in format of the following:
    // finance_customer_transactions-yyyymmddhhmmss.csv

    if ((InputFileName.startsWith("finance_customer_transactions-")) && (InputFileName.endsWith(".csv")) && (IFlength == 48))
    {
                // filename has correct base and extension.  Get date from filename - assume time stamp is in format of yyyymmddhhmmss.
       IFdatetime = InputFileName.substring(31,44);
       filepath = BasePath + "/pending/" + InputFileName;
    }
    else
    {
       // issue error that input file is not correct format - stop program with error code
       System.out.println("Error:Input file is not correct.  The input file should be in the following format: ");
       System.out.println("finance_customer_transactions-yyyymmddhhmmss.csv");
       System.out.println("Please check input file.  Program is exiting");
       System.exit(1);
    }
           

   // Create output filename.

   String OutputFileName = "finance_customer_transactions_report-" + IFdatetime + ".txt";
   String Outputfilepath = BasePath + "/reports/" + OutputFileName;

   
   String line = null;                // parameter for reading file input to
   boolean IsFirstLine = true;        // flag to indicate it is first line - so that if there is a header - it can be ignored.
   int rec_count = 0;                 // counter to ensure that the input file is not more than 500,000 records
   
   try
   {  
                      // input files declaration
      FileReader fin = new FileReader(filepath);
      BufferedReader reader = new BufferedReader(fin);  

                      // output files declaration
       FileWriter fout = new FileWriter(Outputfilepath);
       BufferedWriter writer = new BufferedWriter(fout);    
      
      // process file

      while (((line = reader.readLine()) != null) && (rec_count <= 500000))
      {
                // check first line - if it is title - then ignore.
         rec_count++;
         if (rec_count > 500000)
         {
             System.out.println("Error: Input file has more than 500,000 records.");
             System.out.println("Maximum record size of input file is 500,000.");
             System.exit(1);
         }
         String delimiter = ",";
         String [] fields = line.split(delimiter);
         int fieldsize = fields.length;

              //there should be 2 fields - field1 - account and field 2 - amount.
              // check for validity and update counts.

         if (fieldsize > 0)
         {
             // remove any spaces/tabs from field
            
             fields[0] = fields[0].replaceAll("\\s","");
            
                         // check that account number is all numerals

            if (CheckNumber(fields[0]))
            {
                      // just in case input file is missing header - if there is a valid transaction, it is no longer first line
                IsFirstLine = false;

               // account number is a number.  Check if amount is also valid.
              
               if (fieldsize > 1) 
               {
                  // remove any whitespace from field

                  fields[1] = fields[1].replaceAll("\\s","");
                  
                       // check if amount is valid

                  if (CheckDecimal(fields[1]))
                  {
                     // amount is a decimal value - update debit/credit amounts
                     
                     // check if amount is positive or negative

                     if (fields[1].charAt(0) == '-')
                     {
                                 // amount is negative - add absolute value to debit sum
                        String newAmt = fields[1].substring(1);
                        double value = Double.parseDouble(newAmt);
                        Debit_sum = Debit_sum + value;
                     }
                     else
                     {
                                 // amount is positive - add to credit sum
                        double value = Double.parseDouble(fields[1]);
                        Credit_sum = Credit_sum + value;
                     }

                                 // update AccNo_count - if not a duplicate 
                                 //find first number of account

                     char acc = fields[0].charAt(0);
                     int accfirst = Character.getNumericValue(acc);                   
                     
                                     // check account arrays to see whether they already hold the account number
                      if ((accfirst >= 0) && (accfirst < 10))
                      {                                      
                         int i = 0;
                         boolean isFound = false;
                       
                         while ((i < arraysize[accfirst]) && !isFound) 
                         {
                                       // accounts are stored in the array based on first number of the account
                                       // check the appropriate array to determine if account number has already been processed  

                            switch (accfirst)
                            {
                               case 0: 
                                  if (accounts0[i].equals(fields[0]))
                                  {
                                     isFound = true;
                                  }
                                  break;
                               case 1:
                                  if (accounts1[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 2:
                                  if (accounts2[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 3:
                                  if (accounts3[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 4:
                                  if (accounts4[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 5:
                                  if (accounts5[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 6:
                                  if (accounts6[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 7:
                                  if (accounts7[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 8:
                                  if (accounts8[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               case 9:
                                  if (accounts9[i].equals(fields[0]))
                                  {
                                      isFound = true;
                                  }
                                  break;
                               default:
                                   System.out.println("Error: account does not start with a number.  Please check");
                                   System.exit(1);
                                   break;
                            }  // switch end
                                        // increment counter for next while iteration.
                            i++;

                         }    // while end

                                        // if account is not found, add the account number to the array and update the array counter.
                        if (!isFound)
                         {
                                                  // add account number to the relevant account array if not found
                            switch (accfirst)
                            {
                               case 0: 
                                  accounts0[arraysize[accfirst]] = "";
                                  accounts0[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 1:
                                  accounts1[arraysize[accfirst]] = "";
                                  accounts1[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 2:
                                  accounts2[arraysize[accfirst]] = "";
                                  accounts2[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 3:
                                  accounts3[arraysize[accfirst]] = "";
                                  accounts3[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 4:
                                  accounts4[arraysize[accfirst]] = "";
                                  accounts4[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 5:
                                  accounts5[arraysize[accfirst]] = "";
                                  accounts5[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 6:
                                  accounts6[arraysize[accfirst]] = "";
                                  accounts6[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 7:
                                  accounts7[arraysize[accfirst]] = "";
                                  accounts7[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 8:
                                  accounts8[arraysize[accfirst]] = "";
                                  accounts8[arraysize[accfirst]] = fields[0];    
                                  break;
                               case 9:
                                  accounts9[arraysize[accfirst]] = "";
                                  accounts9[arraysize[accfirst]] = fields[0];    
                                  break;
                               default:
                                   System.out.println("Error: account does not start with a number.  Please check");
                                   break;
                            }         // end of switch
                            arraysize[accfirst] = arraysize[accfirst] + 1;
                            AccNo_count++;                                 
                         }   // if not found
                           
                       }  // if accfirst is within bounds
    
                  }
                  else
                  {
                          //field is not a valid amount - skip record an update count
                     Skip_count++;
                  }
               }
               else
               {
                  // record is not valid as there is no amount
                  Skip_count++;
               }
            }
            else
            {
                  // account number is not a number - first line will always be a non-number - do not add to skip_count
                if (IsFirstLine)
                {
                    IsFirstLine = false;
                }
                else
                {
                   Skip_count++;
                }
            }
         }
           
      }
                      // write output to file

         DecimalFormat DecFormatter = new DecimalFormat("$###,###.###");
         DecimalFormat NumFormatter = new DecimalFormat("###,###.###");
         String output = "";
 
         
         writer.write("File Processed: " + InputFileName);
         writer.newLine();
         output = NumFormatter.format(AccNo_count);
         writer.write("Total Accounts: " + output);
         writer.newLine();
         output = "";
         output = DecFormatter.format(Credit_sum);
         writer.write("Total Credits : " + output);
         writer.newLine();
         output = "";
         output = DecFormatter.format(Debit_sum);
         writer.write("Total Debits  : " + output);
         writer.newLine();
         output = "";
         output = NumFormatter.format(Skip_count);
         writer.write("Skipped Transactions: " + Skip_count);

      // close file

     reader.close();
     writer.close();     
   }           
   catch(FileNotFoundException ex)
   {
      System.out.println("Error: Unable to open file ");
      ex.printStackTrace();
      System.exit(1);
   }
   catch (IOException ex)
   {
      System.out.println("Error: Unable to read/write to file ");
      ex.printStackTrace();
      System.exit(1);
   }        
}

}
